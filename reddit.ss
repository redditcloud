(module reddit mzscheme
  (require (lib "url.ss" "net")
           (lib "cgi.ss" "net")
           (lib "serialize.ss")
           (only (lib "string.ss") regexp-split)
           (only (lib "13.ss" "srfi") string-contains string-join)
           (planet "sxml.ss" ("lizorkin" "sxml.plt" 1 4))
           (planet "ssax.ss" ("lizorkin" "ssax.plt" 1 3))
           (planet "htmlprag.ss" ("neil" "htmlprag.plt" 1 3)))
  
  (require "cache.ss")
  (require "web.ss")
  (require "json/json.ss")
  
  (define (subreddit-to-url subreddit)
    (if subreddit
        (format "http://~a.reddit.com/" subreddit)
        "http://reddit.com/"))
  
  (define string-strip
    (let ((r-strip (regexp "^[ \t\r\n]*(.*?)[ \t\r\n]*$")))
      (lambda (s)
        (cadr (regexp-match r-strip s)))))
  
  (define (url->sxml url)
    (html->sxml (get-pure-port (string->url url))))
  
  (define (is-url? string)
    (string-contains string "http://"))
  
  ; Guess time duration from human readable string
  ; > (guess-time "7 Days 21 hours")
  ; 680400
  (define (guess-time human-readable)
    (let loop ((patterns 
                (list "([0-9]+)+ Days ([0-9]+)+ hours"
                      (lambda (e) (+ (* 24 60 60 (e 1))
                                     (* 60 60 (e 2))))
                      "([0-9]+)+ hours"
                      (lambda (e) (* 60 60 (e 1)))
                      "([0-9]+)+ days"
                      (lambda (e) (* 24 60 60 (e 1)))
                      "1 day"
                      (lambda (e) (* 24 60 60)))))
      (let ((m   (regexp-match (car patterns) human-readable))
            (ext (cadr patterns)))
        (if m
            (ext (lambda (i) (string->number (list-ref m i))))
            (loop (cddr patterns))))))
  
  ; (join-url "http://reddit.com/" "/info/foo")
  ; ==> "http://reddit.com/info/foo"
  (define (join-url domain path)
    (string-append (substring domain 0 (- (string-length domain) 1))
                   path))
  
  ; (conver-back-url "http://foo.com/?a=1&amp;b=2&amp;c=3")
  ; ==> "http://foo.com/?a=1&b=3&c=3
  (define (convert-back-url quoted-url)
    (regexp-replace* "&amp;" quoted-url "\\&"))
  
  (define-memoize (get-hot-links subreddit)
    (let ((url (subreddit-to-url subreddit)))
      (let loop ((tr-list 
                  ((sxpath "//table[@id='siteTable']/tr[contains(@id, 'thing')]") (url->sxml url))))
        (if (null? tr-list) '()
            (let* ((itable     ((sxpath "//table/tr") (car tr-list)))
                   (rank*      ((sxpath "//td[@class='numbercol']/text()") (car tr-list)))
                   (row1       (car itable))
                   (row2       (cadr itable))
                   (link*      ((sxpath "//td[@colspan = 3]/a/@href/text()") row1))
                   (title*     ((sxpath "//td[@colspan = 3]/a/text()") row1))
                   (c-href*    ((sxpath "//td[@colspan = 3]/span/a[@class='bylink']/@href/text()") row2))
                   (score*     ((sxpath "//td[@colspan = 3]/span[contains(@id, 'score')]/text()") row2))
                   (comments*  ((sxpath "//td[@colspan = 3]/span/a[@class='bylink']/text()") row2))
                   (age*       ((sxpath "//td[@colspan = 3]/text()") row2))
                   (user*      ((sxpath "//td[@colspan = 3]/a[contains(@href, 'user')]/text()") row2))
                   (top*       ((sxpath "//td[@rowspan = 3]/span/@class/text()") row1))
                   (rlink      (make-hash-table)))
              ;(display (format "row2: ~a" row2))
              (map (lambda (args) (apply hash-table-put! (cons rlink args)))
                   `((href          ,(convert-back-url (car link*)))
                     (title         ,(string-strip (car title*)))
                     (score         ,(if (null? score*) #f ; link submitted < 1 hr ?
                                         (string->number 
                                          (car (regexp-match "[0-9]+"
                                                             (car score*))))))
                     (comments      ,(string->number 
                                      (car (or (regexp-match "[0-9]+"
                                                             (car comments*))
                                               '("0")))))
                     (comments-href ,(convert-back-url 
                                      (if (is-url? (car c-href*))
                                          (car c-href*)
                                          (join-url url (car c-href*)))))
                     (age           ,(let ((m (regexp-match "posted (.+) ago" 
                                                            (cadddr age*))))
                                       (if m
                                           (guess-time (cadr m))
                                           #f)))
                     (user          ,(string-strip (string-join user*)))
                     (top           ,(not (null? top*)))
                     (rank          ,(string-strip (string-join rank*)))))
              (cons rlink
                    (loop (cdr tr-list))))))))
  
  (define (run)
    (run-with-urls 
     ("^/api/(.*)$"  
      (delegate
       (""     (lambda () 
                 (display "Go for /api/hot?sub=science")))
       ("hot"  (lambda ()
                 (let ((sub-reddits
                        (let ((GET (get-bindings/get)))
                          (if GET
                              (map (lambda (s)
                                     (if (string=? s "")
                                         #f
                                         s))
                                   (regexp-split "," (extract-binding/single "sub" GET)))
                              (list #f)))))
                   (json-write (if (= (length sub-reddits) 1)
                                   (get-hot-links (car sub-reddits))
                                   (map get-hot-links sub-reddits))))))))))
  
  (provide run guess-time get-hot-links))