(module cache mzscheme
  (require (lib "serialize.ss")
           (only (lib "19.ss" "srfi") time-second current-time))
  
  (define cache-file-path "/tmp/reddit-cache")
  (define TIMEOUT (* 5 60))
  
  ; TODO: add support for more than one `arg's
  (define-syntax define-memoize
    (syntax-rules ()
      ((_ (name arg) . body)
       (define (name arg)
         (deserialize 
          (cache-get arg
                     (lambda ()
                       (cache-put arg 
                                  (serialize ((lambda () . body)))))))))))
  
  (define (now)
    (time-second (current-time)))
  
  (define (cache-read)
    (if (file-exists? cache-file-path)
        (call-with-input-file cache-file-path
          (lambda (i) (read i)))
        '()))
  
  (define (cache-write alist)
    (if (file-exists? cache-file-path)
        (delete-file cache-file-path))
    (call-with-output-file cache-file-path
      (lambda (o) (write alist o))))
  
  (define (cache-get key thunk)
    (let* ((alist (cache-read))
           (mat   (assoc key alist)))
      (if (and mat
               (> (+ (cadr mat) TIMEOUT)
                  (now)))
          (caddr mat)
          (thunk))))
  
  (define (cache-put key value)
    (let* ((alist (cache-read))
           (mat   (assoc key alist)))
      (if mat
          (cache-write (begin
                         (set-cdr! mat (list (now) value))
                         alist))
          (cache-write (cons (list key (now) value) alist)))
      value))
  
  
  (provide define-memoize))
